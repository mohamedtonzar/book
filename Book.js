const faker = require('faker');
const fs = require('fs')

function generateUsers() {

  let Books = []

  for (let id=1; id <= 1000000; id++) {

    let firstName = faker.name.firstName();
    let lastName = faker.name.lastName();
    let genders = [ 'female' , 'male' ];
    let BooksGender = ['romance','fiction','fantasy','history','horror','finance'];
    let gender = faker.random.arrayElement(genders); 
    let genderBook =faker.random.arrayElement(BooksGender); 

    let date = faker.date.past();
    Books.push({
        "id": id,
        "name": firstName,
        "gender": genderBook,
        "author":{
             "name":lastName ,
             "gender": gender,
        },
        "date": date
    });
  }

  return { "data": Books }
}

let dataObj = generateUsers();

fs.writeFileSync('data.json', JSON.stringify(dataObj, null, '\t'));


